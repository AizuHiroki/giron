@foreach ($comments as $comment)
    <div style="font-size:medium;margin:10px;width:400px;border-left: solid 1px #C0C0F0;">
        {{{$comment->body}}}
            @php
            $viewid = md5(rand(1,1000000));
            $formid = md5(rand(1,1000000));
            $formdivid = md5(rand(1,1000000));
            @endphp
            <span class="res" data-id="{{$comment->id}}" data-form="#{{$formdivid}}" data-channel="{{$channel}}" style="cursor:pointer;text-decoration: underline;color:#0000FF;font-size:small">[res]</span>
            <div id="{{$formdivid}}" style="display:none;">
                <form id ="{{$formid}}" name="res" method="post" action="{{route('comment.resstore')}}">
                    res : <input type="text" class="resinput" id="rescomment" name="rescomment" data-id="#{{$viewid}}" data-form="#{{$formid}}"></input>
                    <input type="text" name="dummy"  style="display:none;"></input>
                    <input type="hidden" id="channel" name="channel" value="{{{$channel}}}"></input>
                    <input type="hidden" id="id" name="id" value="{{{$comment->id}}}"></input>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                </form>
            </div>
            <div id="{{$viewid}}">
                @if ($comment->tc)
                    <span class="resplus" data-id="#{{$viewid}}" data-comment_id="{{$comment->id}}" data-channel="{{$channel}}" style="cursor:pointer;text-decoration: underline;color:#0000FF;font-size:small">[+]</span>
                @endif
            </div>
    </div>
@endforeach

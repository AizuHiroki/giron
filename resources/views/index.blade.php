<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{{$channel}}} chat</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    </head>
    <body style="font-size:medium;background-color:#EEEEFF;">
    <div>
        current channel : {{{$channel}}} (ttl:{{$ttl}}sec)
    </div>
    <hr/>
        <form id ="f1" name="f1" method="post" action="{{route('comment.store')}}">
            comment : <input type="text" id="comment" name="comment"></input>
            <input type="hidden" id="channel" name="channel" value="{{{$channel}}}"></input>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>
@include('list')
    <hr/>
othoer channels : 
@foreach ($ranking as $cn => $rank)
        <a href="/comment?channel={{{$cn}}}">{{{$cn}}}({{$rank}})</a>
@endforeach
    <hr/>
    <div>
        <form id ="fc" name="fc" method="get" action="{{route('comment.index')}}">
            move channel:<input type="text" id="channel" name="channel"></input>
        </form>
    </div>
    <script type="text/javascript">
    $( '#comment' ).focus();

    function init(pref)
    {
        if(pref == undefined)pref = "";
        pref = pref + " ";
/*
        $(pref + '.resplus' ).click(function(){
            var channel = 'res:'+$(this).data("channel")+":"+$(this).data("comment_id");
            $.ajax({
                        url: '/comment/list',
                        context: this,
                        data: { channel: channel},
                        success: function(data){
                                    $(this).hide();
                                    $($(this).data("id")).html(data);
                                    init($(this).data("id"));
                                }
            });
        });
*/
        $(pref + '.resplus' ).each(function(){
            var channel = 'res:'+$(this).data("channel")+":"+$(this).data("comment_id");
            $.ajax({
                        url: '/comment/list',
                        context: this,
                        data: { channel: channel},
                        success: function(data){
                                    $(this).hide();
                                    $($(this).data("id")).html(data);
                                    init($(this).data("id"));
                                }
            });
        });

        $(pref+ '.res' ).click(function(){
            var formid = $(this).data('form');
            $(formid).show();
            $(this).hide();
        });


        $(pref+'.resinput').keypress(function (e) {
            if (e.keyCode == 13){
                var formid = $(this).data('form');
                $.ajax({
                   type: 'POST',
                   url: '/comment/resstore',
                   context: this,
                   data: $(formid).serialize(),
                   success: function(data){
                                $(this).hide();
                                $($(this).data("id")).html(data);
                                init($(this).data("id"));
                           }
                });
                var formid = $(this).data('form');
                $(formid).hide();
            }
        });


    }
    init();
    </script>
    </body>
</html>

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Redis;
use App\Entities\Comment;

class CommentController extends Controller
{

    public function index(Request $r)
    {
        $r->validate([
            'channel' => 'max:1024',
        ]);
        if($r->channel == "")return redirect()->route('comment.index',['channel'=>'default']);
        //$comments = Redis::lrange($r->channel,0,29);
        $comments = Comment::get($r->channel);
        $ttl = Comment::getTtl($r->channel);
        //$ranking = Redis::zRevRange('ranking', 0, 29, 'WITHSCORES');
        $ranking = Comment::getRanking($r->channel);
        return view('index')
                ->withChannel($r->channel)
                ->withComments($comments)
                ->withRanking($ranking)
                ->withTtl($ttl)
                ;
    }

    public function show(Request $r)
    {
        $r->validate([
            'channel' => 'max:1024|required',
        ]);
        return $this->list($r->channel);
    }

    private function list($channel)
    {
        $comments = Comment::get($channel);
        $ttl = Comment::getTtl($channel);
        return view('list')
                ->withChannel($channel)
                ->withComments($comments)
                ->withTtl($ttl)
                ;
    }



    public function store(Request $r)
    {
        $r->validate([
            'channel' => 'max:1024',
            'comment' => 'max:4096',
        ]);
        $c = new Comment();
        $c->tc = false;
        $c->body = $r->comment;
        $c->save($r->channel);
        Comment::addRanking($r->channel);
        return redirect()->back();
    }

    public function resstore(Request $r)
    {
        $r->validate([
            'id'         => 'max:1024|required',
            'channel'    => 'max:1024|required',
            'rescomment' => 'max:4096|required',
        ]);
        $newchannel = "res:{$r->channel}:{$r->id}";
        $c = new Comment();
        $c->tc = false;
        $c->body = $r->rescomment;
        $c->save($newchannel);

        $metajson = Redis::get('meta:'.$r->channel);
        $meta = json_decode($metajson,true);
        $res = array_unique(array_merge((array)$meta['res'],[$r->id]));
//元々のメタに自分のレス番に返信あるよ、とマーク。
        Redis::set('meta:'.$r->channel,json_encode([
                            'fchannel' => null,
                            'fid'      => null,
                            'res'      => $res,
                        ]));
//新しい返信ちぇねる用メタ生成
        $exist = Redis::get('meta:'.$newchannel);
        if(empty($exist)){
            Redis::set('meta:'.$newchannel,json_encode([
                                'fchannel' => $r->channel,
                                'fid'      => $r->id,
                                'res'      => [],
                            ]));
        }
        return $this->list($newchannel);
    }


}

<?php
namespace App\Entities;

use Illuminate\Support\Facades\Redis;

class Comment
{

    public $body = null;
    public $id = 0;
    public $tc = false;

    public function __construct($json = null)
    {
        if(!is_null($json))$this->fromJson($json);
    }

    public function __toString()
    {
        return (string)$this->body;
    }

    public function getFromAttribute()
    {
       /* 
                'fchannel' => $this->,
                'fid'      => $this->,
        */
    }

    public function fromJson($json)
    {
        $ar = json_decode($json,true);
        $this->body = $ar['body'];
        $this->id   = $ar['id'];     
        $this->tc   = $ar['tc'];
        return $this;
    }


    public function toJson()
    {
        return json_encode([
                'body'     => $this->body,
                'id'       => $this->id,
                'tc'       => $this->tc,
            ]);
    }

    /*
    * ranking
    */
    public static function addRanking($channel)
    {
        Redis::zIncrby("ranking",1,$channel);
    }

    public static function getRanking()
    {
        return Redis::zRevRange('ranking', 0, 29, 'WITHSCORES');
    }


    /*
    * comment
    */
    public function save($channel)
    {
        $len = Redis::llen($channel);
        $this->id = md5(uniqid());
        Redis::lpush($channel,$this->toJson());
        Redis::expire($channel,60*60*24*30);
    }

    public static function get(string $channel)
    {
        $metajson = Redis::get('meta:'.$channel);
        $meta = json_decode($metajson,true);

        $rets = Redis::lrange($channel,0,29);
        $comments = collect();
        foreach($rets as $ret){
            $c = new Comment($ret);
            $c->tc = in_array($c->id,(array)$meta['res']);
            $comments->push($c);
        }
        return collect($comments);
    }

    public function res($comment)
    {
        $this->tc = true;
        Redis::lpush($channel,$this->body);
        Redis::zIncrby("ranking",1,$channel);
        Redis::expire($channel,60*60*24*30);
    }




    public static function getTtl($channel)
    {
        return Redis::ttl($channel);
    }
}
